.TH CLM 1 "August 2019" "Version 3.0"
.SH NAME
clm \- Clean compiler (make)
.SH SYNOPSIS
clm -V

clm ---version

clm [options] main_module_name [-o application_name]
.SH DESCRIPTION
Makes an application from clean (.icl and .dcl) file(s). The main_module_name
should be specified without (.icl) extension.
  
If '-o application_name' is not used, the name of the application is 'a.out'.
(and is stored in the working directory)
.SH CONFIGURATION OPTIONS
.IP -V
print version information and variables.
.IP --version
print version information.
.IP -I\ path
append `path` to the search path.
.IP -IL\ path
prepend the library path to `path` and append the result to the search path.
.IP -P\ pathlist
search path is `pathlist`.
.IP -mv
verbose output of the make process.
.IP -ms
silent make process.
.IP -clc\ file
Use <file> as the compiler executable.
.IP -aC,option(s)
pass comma separated <option(s)> to the compiler (e.g. to set the compiler's
heap size: -aC,-h,100m).
.IP -RE\ file
redirect the compiler stderror to <file>
.IP -RO\ file
redirect the compiler stdout to <file>
.IP -j\ [n]
parallelize jobs over <n> processors (if n is not given, the total number of processors is used)
.SH BUILD OPTIONS
These options determine which build steps are performed.
.IP -c
only check the syntax of the main module.
.IP -ABC
only generate abc (.abc) file of the main module.
.IP -S
only generate assembly (.s) file of the main module.
.IP -O
only generate object (.o) file of the main module.
.IP -PABC
only generate abc (.abc) files of modules in the project.
.IP -PS
only generate assembly (.s) files of modules in the project.
.IP -PO
only generate object (.o) files of modules in the project.
.SH COMPILATION OPTIONS
These options apply whenever a module is recompiled, but do not trigger recompilation themselves.
.IP -aui\ -naui
enable/disable allowance of undecidable instances (NB: may result in
non-termination of the compiler). (default: -naui)
.IP -ci\ -nci
enable/disable array indices checking. (default: -nci)
.IP -lt\ -nlt
enable/disable listing only the inferred types. (default -nlt)
.IP -lat\ -nlat
enable/disable listing all the types. (default: -nlat)
.IP -lset
list the types of functions for which not all strictness information has been exported.
.IP -funcmayfail
enable the function totality checker.
.IP -warnfuncmayfail
enable the function totality checker but only emit it as a warning.
.SH PROJECT OPTIONS
These options apply to the whole project.
If a module was previously compiled with different options, it is recompiled.
.IP -dynamics\ -ndynamics
enable/disable support for dynamics. (default: -ndynamics)
.IP -tst
enable the generation of code for stack tracing. (default disabled)
.IP -pt\ -npt
enable/disable the generation of code for time profiling. (default: -npt)
.IP -pg\ -npg
enable/disable the generation of code for callgraph profiling. (default: -npg)
.IP -desc
generate all descriptors.
.IP -exl
export local labels.
.IP -optabc
generate optimized ABC code (requires the ABC interpreter).
.IP -bytecode
generate interpreter bytecode (requires the ABC interpreter).
.IP -prelink-bytecode
generate prelinked interpreter bytecode, for the WebAssembly interpreter (requires the ABC interpreter).
.IP -pic
only on ARM: generate position-independent code.
.SH MAIN MODULE OPTIONS
These options are applied to the main module only.
When it was compiled with different options it is recompiled.
The other modules in the project are not affected.
.IP -w\ -nw
enable/disable warnings. (default: -w)
.IP -d\ -nd
enable/disable the generation of readable label names. (default: -nd)
.IP -sa\ -nsa
enable/disable strictness analysis. (default: -sa)
.IP -ou\ -nou
enable/disable optimizing uniqueness typing. (default: -ou)
.IP -fusion\ -nfusion
enable/disable optimizing by fusion transformation. (default: -nfusion)
.IP -generic_fusion\ -ngeneric_fusion
enable/disable optimizing by generic fusion transformation. (default: -ngeneric_fusion)
.SH LIFTING MAIN MODULE OPTIONS TO PROJECT OPTIONS
The following options 'lift' main module options to project options.
They are then applied to all modules in the project.

For instance, use -Pw -w to enable warnings on all project modules, not just the main module.
Use -Pw -nw to disable warnings on all project modules.
.IP -Pw
lifts -w/-nw to a project option.
.IP -Pd
lifts -d/-nd to a project option.
.IP -Psa
lifts -sa/-nsa to a project option.
.IP -Pou
lifts -ou/-nou to a project option.
.IP -Pfusion
lifts -fusion/-nfusion to a project option.
.IP -Pgeneric_fusion
lifts -generic_fusion/-ngeneric_fusion to a project option.
.SH LINKER OPTIONS
.IP -ns
disable stripping the application.
.IP -no-opt-link
disable the optimizing linker.
.IP -l\ object_file
include the file <object_file>.
.IP -sl\ file
include the shared library or linked option file (SEE LINK OPTIONS FILES) <file>.
.IP -e\ symbol
export the label name <symbol> for shared libraries.
.IP -E\ file
same as -e, but the exported label names are specified in the file <file>,
separated by newlines.
.IP -post-link file
After linking, run $CLEANLIB/file as a post link stage.
The arguments are object files of all included modules (but not _startup.o and the like), and -o followed by the output file.
.SH APPLICATION OPTIONS
These options do not trigger recompilation but modify behaviour of the application.
They can be given to
.BR clm ,
to change the default for the application, or to the application itself during execution (unless it was built with
.BR -nortsopts ).
.IP -h\ size
set the heap size to size in bytes (note: append k, K, m, or M to denote
kilobytes or megabytes). (default: 2m)
.IP -s\ size
set the stack size to size in bytes (note: append k, K, m, or M to denote
kilobytes or megabytes). (default: 512k)
.IP -b\ -sc
display the basic values or the constructiors. (default: -sc)
.IP -t\ -nt
enable/disable displaying the execution times. (default: -t)
.IP -gc -ngc
enable/disable displaying the heap size after garbage collection. (default:
-ngc)
.IP -st\ -nst
enable/disable displaying the stack size before garbage collection. (default:
-nst)
.IP -nr
disable displaying the result of the application.
.IP -gcm
use marking/compacting garbage collection.
.IP -gcc
use copy/compacting garbage collection.
.IP -gcf\ n
multiply the heap size with <n> after garbage collection. (default: 20)
.IP -gci\ size
set the initial heap size to <size> in bytes (note: append k, K, m, or M to denote
kilobytes or megabytes). (default: 100k)
.IP -nortsopts
disable RTS command line arguments in the generated application.
.SH MAKING A PROJECT
Clm makes a project in the following way:
.IP 1.
All .icl files in the project which are no longer up to date are 
compiled. The .icl files in the project are the main module and all 
modules imported (directly and indirectly) by this module, except 
system modules. A .icl file is considered not up to date if:
.RS
.IP \-
no corresponding .abc file exists for this module.
.IP \-
the .abc file contains parallel code, does not contain stack lay
out information or is generated with an other compiler version.
.IP \-
the .abc file is older than a corresponding .icl or .dcl file.
.IP \-
the .abc file is compiled with different
.BR PROJECT\ OPTIONS .
.IP \-
the .icl file is the main module and the .abc file is compiled
with different
.BR MAIN\ MODULE\ OPTIONS .
.IP \-
the .abc file is older than any of the .dcl files which are imported
by this module.
.LP
If a module has to be compiled the following compiler options are used:
.IP \-
for the main module: the options specified to clm.
.IP \-
for other modules: if a corresponding .abc file exists, the options
which were use to generate this .abc file, otherwise the default
options. So the compiler options are remembered (in the abc file)
after the first successfull compilation. If you want to change
the options for such modules, compile the module with -ABC, -S or -O
and the required options. Then, if the compilation succeeds, the
options are remembered.
.RE
.IP 2.
Code is generated for all .abc files in the project which are no longer
up to date. The .abc files in the project are all .abc files for which a
corresponding clean module exists which is the main module, or is 
imported (directly or indirectly) by the main module. A .abc file is
considered not up to date if:
.RS
.IP \-
no corresponding .o file exists.
.IP \-
the corresponding .o file is older than the .abc file.
.LP
After generating code for a module, the assembler is called.
.RE
.IP 3.
An application is build using the linker.
.SH FINDING FILES
.LP
Clm searches files in the directories specified by the CLEANPATH 
environment variable. If the file is not found in any of these directories,
it searches the clean library directory, then the working directory. 
CLEANPATH should contains a list of directories seperated by ':'.
.LP
The .abc, .s, .o(bj), and .bc files are normally generated in a directory
\'Clean System Files\' in the same directory as the directory which contains
the .icl file.
.LP
It is possible to use a custom directory (for all generated files) by setting
the CLM_ARTIFACTS_PATH environment variable.

.SH LINK OPTION FILES
Link option files consist of two parts separated by an
= sign.
.LP
The first section consists of options that are passed on
to the linker by clm.
.LP
The second section lists all the modules that clm should *not*
pass to the linker, because they are already combined in
one of the libraries mentioned in the first section.
clm does not check if these modules are up-to-date.
.LP
The names between parentheses are the modules on which the
the first module depends.
