void start_n_caching_compilers_with_args (int n,void(callback)(int,void*),char *cocl_path,char *cocl_argv[],int cocl_argv_size);
void start_n_caching_compilers (int n,void(callback)(int,void*),char *compiler_path);
int stop_caching_compilers (void);
int wait_for_free_caching_compiler (int return_immediately);
void call_caching_compiler (void *arg,char *args);
int caching_compiler_busy (void);
