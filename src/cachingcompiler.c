/*
	File:		cachingcompiler.c
	Written by: Ronny Wichers Schreur
	At:			University of Nijmegen
*/

#include "cachingcompiler.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#if defined(I486) || defined (SOLARIS) || defined (LINUX)
#	include <unistd.h>
#endif

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

#ifdef DEBUG
static void log_error (char *format, ...)
{
	va_list ap;

	va_start(ap, format);
	(void) vfprintf(stderr, format, ap);
	va_end(ap);
	fflush (stderr);
}
#else
# define log_error(...)
#endif

static int wait_for_child (pid_t pid, char *child_name, int *status_p)
{
    int result, stat;
	pid_t	return_pid;

	return_pid = waitpid (pid, &stat, 0);

	if (return_pid == -1)
		/* child exited before call to waitpid */
		return 0;

	if (return_pid != pid)
	{
		fprintf (stderr, "wait_for_child: unexpected pid result (%d)\n", (int) return_pid);
		exit (1);
	}

	*status_p=stat;
	result = WEXITSTATUS (stat);

#ifdef DEBUG
	if (WIFSIGNALED(stat) != 0)
       	log_error ("%s signaled (%d)\n",child_name, (int) WTERMSIG(stat));
	else if (WIFEXITED (stat))
		log_error ("%s exited normally (%d)\n", child_name, (int) WEXITSTATUS(stat));
	else
       	log_error ("%s exited abnormally (%d)\n",child_name, (int) result);
#endif

	return result;
}

static void error (char *error_string)
{
	fprintf (stderr,"%s\n",error_string);
}

struct compiler {
	pid_t compiler_pid;
	char compiler_commands_name[16];
	char compiler_results_name[16];
	FILE *compiler_commands;
	FILE *compiler_results;
	int compiler_results_fd;
	int compiler_free;
	void *compiler_current_argument;
};

static struct compiler *compilers=NULL;
static int n_compilers;

static void(*free_compiler_callback)(int,void*);

static int stop_caching_compiler (struct compiler *compiler)
{
	int r,status;

	log_error ("stop_caching_compiler\n");
	if (compiler->compiler_pid != 0) {
		pid_t	pid;

		pid = compiler->compiler_pid;
		compiler->compiler_pid = 0;

		log_error ("stop_caching_compiler: compiler running\n");

		fputs ("quit\n", compiler->compiler_commands);
		fflush (compiler->compiler_commands);

		r=wait_for_child (pid, "Clean compiler",&status);
		if (r!=0){
			log_error ("r=%d	status=%xd\n", r, status);
			error ("clm: error after stopping compiler");
			exit (1);
		}
	}
	return 0;
}

int stop_caching_compilers (void)
{
	int i,r=0;
	for (i=0; i<n_compilers; i++)
		r|=stop_caching_compiler (&compilers[i]);
	return r;
}

static void cleanup_compiler (struct compiler *compiler)
{
	log_error ("cleanup_compiler\n");

	stop_caching_compiler (compiler);

	log_error ("cleanup_compiler: unlink commands\n");
	if (unlink (compiler->compiler_commands_name) != 0)
		perror ("clm: unlink compiler commands pipe");

	log_error ("cleanup_compiler: unlink results\n");
	if (unlink (compiler->compiler_results_name) != 0)
		perror ("clm: unlink compiler results pipe");
}

static void cleanup_compilers_at_exit (void)
{
	int i;
	log_error ("cleanup_compiler_on_exit\n");

	for (i=0; i<n_compilers; i++)
		cleanup_compiler (&compilers[i]);
}

/* last cocl_argv[1..n_args] contain the n_args args, cocl_argv_size = n_args+5 */
static int start_caching_compiler_with_args (struct compiler *compiler,char *cocl_path,char *cocl_argv[],int cocl_argv_size)
{
	log_error ("start_caching_compiler\n");

	{
		strcpy (compiler->compiler_commands_name,"/tmp/comXXXXXX");
		int fd;
		
		fd=mkstemp (compiler->compiler_commands_name);
		if (fd<0){
			perror ("clm: mkstemp failed");
		}
		close (fd);
		unlink (compiler->compiler_commands_name);
		if (mkfifo(compiler->compiler_commands_name, S_IRUSR | S_IWUSR)) {
			perror("clm: mkfifo compiler commands pipe");
			exit(1);
		}
	}

	{
		strcpy (compiler->compiler_results_name,"/tmp/resXXXXXX");
		int fd;
		
		fd=mkstemp (compiler->compiler_results_name);
		if (fd<0){
			perror ("clm: mkstemp failed");
		}
		close (fd);
		unlink (compiler->compiler_results_name);
		if (mkfifo(compiler->compiler_results_name, S_IRUSR | S_IWUSR)) {
			perror("clm: mkfifo compiler results pipe");
			exit(1);
		}
	}

	compiler->compiler_pid=fork();
	if (compiler->compiler_pid<0)
		error ("Fork failed");
	if (compiler->compiler_pid==0){
		if (cocl_argv==NULL || cocl_argv_size<5)
			execlp (cocl_path, "cocl", "--pipe", compiler->compiler_commands_name,
				compiler->compiler_results_name, (char *) 0);
		else {
			cocl_argv[0]="cocl";
			cocl_argv[cocl_argv_size-4]="--pipe";
			cocl_argv[cocl_argv_size-3]=compiler->compiler_commands_name;
			cocl_argv[cocl_argv_size-2]=compiler->compiler_results_name;
			cocl_argv[cocl_argv_size-1]=(char *)0;
			execvp (cocl_path,cocl_argv);
		}
		
		log_error ("cocl path = %s\n", cocl_path);
		perror ("clm: can't start the clean compiler");
		exit(1);
	}

	do
		compiler->compiler_commands=fopen (compiler->compiler_commands_name, "w");
	while (compiler->compiler_commands==NULL && errno==EINTR);

	if (compiler->compiler_commands==NULL){
		perror("clm: fopen compiler commands pipe");
		exit(1);
	}

	do
		compiler->compiler_results=fopen (compiler->compiler_results_name, "r");
	while (compiler->compiler_results==NULL && errno==EINTR);

	if (compiler->compiler_results==NULL){
		perror("clm: fopen compiler commands pipe");
		exit(1);
	}

	compiler->compiler_results_fd=fileno (compiler->compiler_results);
	compiler->compiler_free=1;
	compiler->compiler_current_argument=NULL;

	return 0;
}

void start_n_caching_compilers_with_args (int n,void(callback)(int,void*),char *cocl_path,char *cocl_argv[],int cocl_argv_size)
{
	if (compilers!=NULL){
		fprintf (stderr,"start_n_caching_compilers_with_args: already initialized\n");
		exit (1);
	}

	if (atexit (cleanup_compilers_at_exit)!=0){
		perror ("clm: atexit install cleanup routine");
		exit (1);
	}

	free_compiler_callback=callback;

	n_compilers=n;
	compilers=calloc (sizeof (struct compiler),n);
	if (compilers==NULL){
		perror ("clm: start_n_caching_compilers_with_args");
		exit (1);
	}

	for (--n; n>=0; n--){
		if (start_caching_compiler_with_args (&compilers[n],cocl_path,cocl_argv,cocl_argv_size)!=0){
			fprintf (stderr,"Failed to start the compiler.\n");
			exit (1);
		}
	}
}

void start_n_caching_compilers (int n,void(callback)(int,void*),char *cocl_path)
{
	start_n_caching_compilers_with_args (n,callback,cocl_path,NULL,0);
}

#define RESULT_SIZE (sizeof (int)+2)

/* if return_immediately=1, return if there is any free compiler.
 * if return_immediately=0, return only after a still running compiler *became*
 *   free.
 */
int wait_for_free_caching_compiler (int return_immediately)
{
	int i,free_i=-1,nfds=0;
	fd_set read_fds;
	FD_ZERO (&read_fds);

	for (i=0; i<n_compilers; i++){
		if (compilers[i].compiler_free){
			if (return_immediately)
				return i;
		} else {
			if (compilers[i].compiler_results_fd>=nfds)
				nfds=compilers[i].compiler_results_fd+1;
			FD_SET (compilers[i].compiler_results_fd,&read_fds);
		}
	}

	if (select (nfds,&read_fds,NULL,NULL,NULL)<=0){
		perror ("clm: select failed");
		exit (1);
	}

	for (i=0; i<n_compilers; i++){
		if (FD_ISSET (compilers[i].compiler_results_fd,&read_fds)){
			int r;
			char result_string[RESULT_SIZE], *end;
			void *arg;

			free_i=i;

			if (fgets(result_string,RESULT_SIZE,compilers[i].compiler_results) == NULL){
				perror ("clm: reading compiler result failed");
				exit (1);
			}

			r=(int)strtol (result_string,&end,0);
			if (*end != '\n'){
				perror ("clm: non integer compiler result");
				exit (1);
			}

			compilers[i].compiler_free=1;
			arg=compilers[i].compiler_current_argument;
			compilers[i].compiler_current_argument=NULL;
			free_compiler_callback (r,arg);
		}
	}

	if (free_i<0){
		fprintf (stderr,"clm: internal error in wait_for_free_caching_compiler\n");
		exit (1);
	}

	return free_i;
}

void call_caching_compiler (void *arg,char *args)
{
	struct compiler *compiler;
#if defined (LINUX)
	void (*oldhandler)(int);

	oldhandler = signal (SIGALRM, SIG_IGN);
#endif

	log_error ("call_caching_compiler\n");

	compiler=&compilers[wait_for_free_caching_compiler(1)];

	if (compiler->compiler_pid == 0)
		error ("call_compiler: compiler not running");
	
	fputs (args,compiler->compiler_commands);
	fputc ('\n',compiler->compiler_commands);
	fflush (compiler->compiler_commands);

	compiler->compiler_free=0;
	compiler->compiler_current_argument=arg;

#if defined (LINUX)
	(void) signal (SIGALRM, oldhandler);
#endif
}

int caching_compiler_busy (void)
{
	int i;

	for (i=0; i<n_compilers; i++)
		if (!compilers[i].compiler_free)
			return 1;

	return 0;
}
