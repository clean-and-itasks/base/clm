# clm

`clm` is a make tool for [Clean][].

This repository is a fork of the [upstream][] found in classic Clean, with
additions for use in [Nitrile][]. Normally you should not need to use it
directly, but use the
[`clm`](https://clean-and-itasks.gitlab.io/nitrile/nitrile-yml/reference#buildscriptclm)
script in `nitrile.yml` instead.

`clm` is released in the `base-clm` package, which is included in `base`.

## Maintainer & license

This fork is maintained by [Camil Staps][].
The upstream is maintained by John van Groningen.

For license details, see the [LICENSE](/LICENSE) file.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[Nitrile]: https://clean-and-itasks.gitlab.io/nitrile
[upstream]: https://gitlab.science.ru.nl/clean-and-itasks/clm
